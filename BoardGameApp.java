import java.util.Scanner;

public class BoardGameApp {
	public static void main (String[] args){
		Scanner reader = new Scanner (System.in);
		Board board = new Board();
		int numOfCastles = 5;
		int turns = 0;
		
		//welcome message
		System.out.println("Welcome to the castle game!\nYou will have 8 turns to place a castle in a position on the board. But beware! There are certain spots with hidden walls!\nHave fun and good luck, you'll need it :) ");
		
		//game loop
		while (turns < 8 && numOfCastles > 0){
			if (turns == 7){
				System.out.println("Last Turn! Be careful!");
			}
			System.out.println(board);
			System.out.println("Number of castles: "+numOfCastles +"\n" +"Number of turns: " +turns);
			
			//user selects position
			System.out.println("Please enter a row number (0-4): " );
			int row = reader.nextInt();
			System.out.println("And a column number (0-4): " );
			int col = reader.nextInt();
			
			//updating board
			int response = board.placeToken(row, col);
			
			if (response == -2 || response == -1){
				System.out.println("Invalid move, please try again! \n");
			}else if (response == 1){
					System.out.println("Oof! There was a wall in that position :( \n");
					turns++;
				}else {
					System.out.println("Castle placed! One step closer to victory! \n");
					numOfCastles--;
					turns++;
				}
			}
			System.out.println(board);
			
			if (numOfCastles == 0){
			System.out.println("Congrats on the win!");
			}else {
			System.out.println("Better luck next time!");
			}
		}
}
