import java.util.Random;

public class Board {
    private Tile[][] grid;
	final int boardSize = 5;
   
   public Board() {
	   Random rng = new Random();
        grid = new Tile[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                grid[i][j] = Tile.BLANK;
            }
        }
		for (int x = 0; x < boardSize; x++){
			int randNum = rng.nextInt(boardSize);
			grid[x][randNum] = Tile.HIDDEN_WALL;
		}
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                sb.append(grid[i][j].getName()).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public int placeToken(int row, int col) {
        if (row < 0 || row > 4 || col < 0 || col > 4) {
            return -2;
        }
        if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
		   return -1;
        }
		if (grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
        
    }
}