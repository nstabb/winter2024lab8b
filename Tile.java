public enum Tile {
    BLANK("_"),
	HIDDEN_WALL("H"),
    WALL("W"),
    CASTLE("C");

    private String name;

    private Tile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
